package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException {
        Class<Park> parkClass = Park.class;
        Park park = null;
        try {
            Constructor<Park> constructor = parkClass.getDeclaredConstructor();
            constructor.setAccessible(true);
            park = (Park) constructor.<Park>newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        List<ParkParsingException.ParkValidationError> validationErrors = new ArrayList<>();

        try {
            FileReader fileReader = new FileReader(parkDatafilePath);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            bufferedReader.readLine();
            List<String[]> fields = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                String string = bufferedReader.readLine();
                String[] strings = string.split(":");
                fields.add(strings);
            }
            for (Field field : parkClass.getClass().getFields()) {
                if (field.getAnnotation(FieldName.class) != null) {
                    String value = "";
                    for (int i = 0; i < fields.size(); i++) {
                        if (fields.get(i)[0].contains(field.getName())) {
                            value = fields.get(i)[1];
                            break;
                        }
                    }
                    for (int i = 0; i < fields.size(); i++) {
                        if (fields.get(i)[0].contains(field.getAnnotation(FieldName.class).value())) {
                            value = fields.get(i)[1];
                            break;
                        }
                    }
                    try {
                        field.set(parkClass, fields.get(1)[1]);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }

                if (field.getAnnotation(MaxLength.class) != null) {
                    String value = "";
                    for (int i = 0; i < fields.size(); i++) {
                        if (fields.get(i)[0].contains(field.getName())) {
                            value = fields.get(i)[1];
                            break;
                        }
                    }
                    if (value.length() > field.getAnnotation(MaxLength.class).value()) {
                        ParkParsingException.ParkValidationError parkValidationError = new ParkParsingException.ParkValidationError(
                                field.getName(), "Length of value is big");
                        validationErrors.add(parkValidationError);
                        continue;
                    }
                    try {
                        field.set(parkClass, fields.get(1)[1]);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!validationErrors.isEmpty()) {
            ParkParsingException parsingException = new ParkParsingException("Error", validationErrors);
            for (int i = 0; i < parsingException.getValidationErrors().size(); i++) {
                System.out.println(parsingException.getValidationErrors().get(i).validationError);
            }
            return null;
        }
        return park;
    }
}
